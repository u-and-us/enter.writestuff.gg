
import Rule from '@laraform/laraform/src/utils/validation/rules/rule'

export default class Uppercase extends Rule {
	get message() {
		return 'The :attribute should only contain uppercase letters'
	}

	validate(value) {
		return value.toUpperCase(value) === value
	}
}