import "babel-polyfill";

import Vue from 'vue'
import Laraform from 'laraform';

Vue.use(Laraform);

import EntryForm from './components/forms/EntryForm';

const app = new Vue({
	el: '#app',
	components: {
		'entry-form': EntryForm
	}
});