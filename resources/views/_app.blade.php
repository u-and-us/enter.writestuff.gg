<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, viewport-fit=cover">
	<title>@yield('title') - WriteStuff {{ config('bwi.year') }}, a writing competition for students</title>

    <link rel="stylesheet" href="https://use.typekit.net/hvd2jej.css">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville&display=swap" rel="stylesheet">
    <link rel="stylesheet" media="all" href="{{ mix('css/app.css') }}">
	<script src="{{ mix('js/app.js') }}" defer></script>

	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicon/apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('img/favicon/site.webmanifest') }}">
	<link rel="mask-icon" href="{{ asset('img/favicon/safari-pinned-tab.svg') }}" color="#358bc2">
	<link rel="shortcut icon" href="{{ asset('img/favicon/favicon.ico') }}">
	<meta name="apple-mobile-web-app-title" content="FREEDOM - WriteStuff 2020, a writing competition for students">
	<meta name="application-name" content="FREEDOM - WriteStuff 2020, a writing competition for students">
	<meta name="msapplication-config" content="{{ asset('img/favicon/browserconfig.xml') }}">
	<meta name="theme-color" content="#358bc2">

	<meta property="og:image" content="{{ asset('img/ogp.png') }}">



@if (config('app.env') === 'local')
		<meta name="robots" content="noindex">
	@else
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-109575016-4"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-109575016-4');
		</script>
	@endif
</head>

<body>
<div id="app" class="page">
	@yield('header')

	@yield('content')

	@include('partials._footer')
</div>
</body>
</html>
