Hi {{ $form['name'] }},

Thank you for entering the {{ config('bwi.year') }} WriteStuff competition.
Your story has been submitted. Entry number: {{ $entryId }}.

Kind regards,

The Guernsey Literary Festival
