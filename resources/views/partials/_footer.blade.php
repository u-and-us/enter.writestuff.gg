<footer class="footer u-w--centred">
    <h2 class="t-4 u-w-narrowest u-mb-50 u-w--centred">
        Each year we set a different brief and receive hundreds of amazing entries
    </h2>

    <div class="briefs u-w-narrow u-mb-40 u-w--centred">
        <a class="u-m-25 briefs__brief" href="https://2022.writestuff.gg/" target="_blank">
            <p class="t-3">2022</p>
            <p class="t-5 fgc-grey">Top Secret</p>
        </a>
        <a class="u-m-25 briefs__brief" href="https://2021.writestuff.gg/" target="_blank">
            <p class="t-3">2021</p>
            <p class="t-5 fgc-grey">Myths & legends</p>
        </a>
        <a class="u-m-25 briefs__brief" href="https://2020.writestuff.gg/" target="_blank">
            <p class="t-3">2020</p>
            <p class="t-5 fgc-grey">Freedom</p>
        </a>
        <a class="u-m-25 briefs__brief" href="https://2019.writestuff.gg/" target="_blank">
            <p class="t-3">2019</p>
            <p class="t-5 fgc-grey">Imaginary worlds</p>
        </a>
        <a class="u-m-25 briefs__brief" href="https://2018.writestuff.gg/" target="_blank">
            <p class="t-3">2018</p>
            <p class="t-5 fgc-grey">Conflict</p>
        </a>
        <a class="u-m-25 briefs__brief" href="https://2017.writestuff.gg/" target="_blank">
            <p class="t-3">2017</p>
            <p class="t-5 fgc-grey">Create a character</p>
        </a>
        <a class="u-m-25 briefs__brief" href="https://2015.writestuff.gg/" target="_blank">
            <p class="t-3">2015</p>
            <p class="t-5 fgc-grey">The suitcase</p>
        </a>
        <a class="u-m-25 briefs__brief briefs__brief--large" href="https://2014.writestuff.gg/" target="_blank">
            <p class="t-3">2014</p>
            <p class="t-5 fgc-grey">Everything you have<br>just told me is a lie</p>
        </a>
    </div>

    <h2 class="t-4 u-w-medium u-mb-50 u-w--centred">
        Partners
    </h2>

    <div class="footer__partners">
        <a href="https://wearebwi.com/">
            <img class="u-mh-40 u-mb-40" src="/img/logos/bwi.svg">
        </a>
        <a href="https://www.library.gg/">
            <img class="u-mh-40 u-mb-40" src="/img/logos/guille-alles.svg">
        </a>
        <a href="https://arts.gg/">
            <img class="u-mh-40 u-mb-40" src="/img/logos/gsy-arts.svg">
        </a>
        <a href="https://islandfamilies.com/">
            <img class="u-mh-40 u-mb-40" src="/img/logos/island-families.svg">
        </a>
    </div>

    <div class="u-mt-60 u-w--centred t-5 footer__print">
        <p class="u-mh-30">Company limited by guarantee: 57768. Guernsey Registered Charity: 455</p>
        <p class="u-mh-30">Website by <a class="fgc-white" href="https://wearebwi.com">Betley Whitehorne Image</a></p>
    </div>

</footer>
