<div class="header t-5 u-w--centred u-mb-30">
    <a href="https://writestuff.gg/">
        <img class="u-mt-30" src="img/logos/writestuff.svg" alt="">
    </a>

    <div class="header__left">
        <p class="u-mb-15">Organised by</p>
        <a href="https://guernseyliteraryfestival.com/">
            <img src="img/logos/glf.svg" alt="">
        </a>
    </div>

    <div class="header__right">
        <p class="u-mb-15">Sponsored by</p>
        <a href="https://www.juliusbaer.com/">
            <img src="img/logos/julius-bar.svg" alt="">
        </a>
    </div>
</div>
