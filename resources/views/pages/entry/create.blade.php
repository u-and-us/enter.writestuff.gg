@extends('_app')

@section('title')
	Enter your story
@endsection

@section('header')
	@include('partials._header')
@endsection

@section('content')
	<main>
        <section class="u-mb-40 u-w--centred u-w-narrow">
            <h1 class="t-1">Submit your story</h1>
        </section>

		<div class="standard-panel u-pb-170 u-mt-80">
			<section class="u-w--centred u-w-narrow entry-form u-mb-150">
				<div class="scope-cms u-mb-50">
					<p>Your story may be up to 300 words long not including the title. Don’t worry we’ll count them for you as you type!</p>

                    <p>Before entering please make sure you read the <a href="https://2023.writestuff.gg/brief" target="_blank">challenge brief</a> and the <a href="https://2023.writestuff.gg/guidelines" target="_blank">guidelines</a>, if you get stuck ask a parent or teacher to help you.</p>
				</div>


				{!! $entry_form->render() !!}
			</section>

			<section class="u-w--centred u-w-narrow scope-cms t-4">
				<h2 class="t--bold u-mb-10">
					Terms and Conditions:
				</h2>

				<ol>
					<li>By submitting my entry to the WriteStuff Competition I am agreeing to my work being uploaded to the Competition website if it wins, or is highly commended. It may also be used for future public displays and&nbsp;online.</li>
					<li>I am a resident in the Bailiwick of Guernsey.</li>
					<li>My age group is as it will be on the closing date of March 10th, 2023.</li>
					<li>My entry is my own unassisted work.</li>
				</ol>

				<p>For further information please contact <a href="mailto:education@guernseyliteraryfestival.com">education@guernseyliteraryfestival.com</a> or call 07781 143545.</p>
			</section>
		</div>
	</main>
@endsection
