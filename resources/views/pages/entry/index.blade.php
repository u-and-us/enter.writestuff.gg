@extends('_app')

@section('title')
	Rules and advice -
@endsection

@section('header')
	@include('partials._header')
@endsection

@section('content')
	<main>
		<div class="standard-panel">
			<section class="u-w-narrow u-w--centred scope-cms">
				<h2 class="t-2 mb-3">Rules and advice</h2>

				<p>Pieces will be judged on creativity, clarity, style and accuracy of language. The aim of the Write Stuff Competition is to develop professional writing skills and further digital literacy.</p>

				<p>The winners will be announced late April and the prizes will be given out at the Competition Awards Ceremony on Saturday May 2nd 2020 by authors Huw Lewis Jones , Neal Layton, Onjali Q. Raúf and Andy Riley.</p>

				<p>All winning and highly commended entries will be loaded onto the Write Stuff website and the winning entries will have pride of place on the website.</p>

				<p>All entries should be submitted directly through the website: www.writestuff.gg</p>

				<p>And please don’t forget! Enter your name, school and age group so that we can identify you.</p>


				<h2 class="t-2 mb-3">Further Advice for both Primary &amp; Secondary School entries:</h2>

				<p>Characters are the most important part of any book, film or play, and in modern fiction, authors tend to drive their characters more through action and dialogue than description, although detailed description can still bring a character to life if handled well.  Try to make your narrator and his or her feelings and emotions believable.</p>


				<h3 class="t-2 mb-3">Dialogue</h3>

				<p>Dialogue is often seen as the most important aspect separating ‘professional’ writing from ‘good’ writing.  Characters will become real for your reader if you show what they say and how they say it. You can also show what they are thinking and any decisions they make. Think about how Dickens portrays Scrooge’s character at the start of “A Christmas Carol” with “Bah Humbug!” Try to include some dialogue to let your character express his/her true self by what they say, how they say it and how it affects others.</p>

				<p>Dialogue should be punctuated inside speech marks, as in the following examples:</p>

				<table class="dialogue">
					<thead>
						<tr>
							<th>Example</th>
							<th>Explanation</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>“This is correct,” he said.</td>
							<td>A comma is used for statements before an attribution.</td>
						</tr>
						<tr>
							<td>“Is this correct?” he asked.</td>
							<td>Questions and exclamations each use their respective marks.</td>
						</tr>
						<tr>
							<td>“This is correct.”</td>
							<td>Statements without attribution end with a full stop.</td>
						</tr>
					</tbody>
				</table>

				<p>Each new piece of dialogue should begin on a new line.</p>

				<p>
					<a href="{{ route('create') }}" class="button button--hot-pink">Enter</a>
				</p>
			</section>
		</div>
	</main>
@endsection



