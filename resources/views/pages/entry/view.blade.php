@extends('_app')

@section('title')
	Thank you -
@endsection

@section('header')
	@include('partials._header')
@endsection

@section('content')
	<main>
		<div class="u-w--centred u-w-narrow">
			<section class="u-mb-30 scope-cms">
				<h1 class="t-1 u-mb-30">Thank you for entering your story {{ $entry->name }}.</h1>

				<p>Good luck with the competition and don’t forget to tell your friends at {{ $entry->school->name }} to enter too!</p>
			</section>

			<p><a href="https://writestuff.gg/" class="button u-mb-100 u-mt-50">Back to the homepage</a></p>
		</div>
	</main>
@endsection
