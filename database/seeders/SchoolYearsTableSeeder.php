<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::table('school_years')->insert([
				['year' => 3, 'category' => 1],
				['year' => 4, 'category' => 1],
				['year' => 5, 'category' => 1],
				['year' => 6, 'category' => 1],
				['year' => 7, 'category' => 2],
				['year' => 8, 'category' => 2],
				['year' => 9, 'category' => 2],
				['year' => '10+', 'category' => 3],
			]);
    }
}
