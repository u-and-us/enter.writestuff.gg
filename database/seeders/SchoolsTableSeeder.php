<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('schools')->insert([
				['name' => 'Amherst School'],
				['name' => 'Blanchelande School'],
				['name' => 'Castel School'],
				['name' => 'Elizabeth College'],
				['name' => 'Forest'],
				['name' => 'Grammar School'],
				['name' => 'Hautes Capelles School'],
				['name' => 'Herm Primary School School'],
				['name' => 'La Houguette School'],
				['name' => 'La Mare de Carteret School'],
				['name' => 'Le Murier School'],
				['name' => 'Le Rondin School'],
				['name' => 'Les Beaucamps School'],
				['name' => 'Les Voies School'],
				['name' => 'Notre Dame School'],
				['name' => 'St Martin’s School'],
				['name' => 'St Sampson’s  School'],
				['name' => 'The Ladies’ College'],
				['name' => 'Vale School'],
				['name' => 'Vauvert School'],
				['name' => 'Other'],
				['name' => 'Home schooled'],
				['name' => 'Sark School'],
				['name' => 'Ladies College, Melrose'],
			]
		);
	}
}
