<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('surname');
            $table->string('email');
            $table->string('title');
			$table->text('story');
			$table->string('title_edited')->nullable();
			$table->text('story_edited')->nullable();
            $table->string('slug');
            $table->integer('winner')->default(0);
			$table->string('soundcloud')->nullable();
			$table->text('comments')->nullable();

			$table->boolean('is_corrected')->default(0);
            $table->boolean('is_published')->default(0);
            $table->boolean('is_judged')->default(0);

            $table->unsignedBigInteger('corrector_id')->default(1);
            $table->unsignedBigInteger('school_id')->default(1);
            $table->unsignedBigInteger('school_year_id')->default(1);
            $table->unsignedBigInteger('judge_id')->default(0);
            $table->unsignedBigInteger('reader_id')->default(0);

            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
