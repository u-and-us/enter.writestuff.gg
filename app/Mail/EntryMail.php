<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EntryMail extends Mailable
{
    use Queueable, SerializesModels;

    public $form;
    private $entryId;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form, $entryId)
    {
        $this->form = $form;
        $this->entryId = $entryId['id'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text('mail.entry-mail')
            ->with([
                'form' => $this->form,
                'entryId' => $this->entryId,
            ])
                ->subject('Thank you for submitting a story');
    }
}
