<?php

namespace App\Http\Controllers;

use App\Mail\EntryMail;
use App\Models\Entry;
use App\Forms\EntryForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EntryController extends Controller
{
	public function create()
	{
		return view('pages.entry.create', [
			'entry_form' => app(EntryForm::class),
		]);
    }

	public function store(Request $request)
	{
        // Obtaining a form instance.
        $form = app(EntryForm::class);

        // First we set the form's data and its primary key, if any.
        $form->setData($request->data);
        $form->setKeyFromData($request->data);

        // Validate the form.
        $form->validate();

        // If invalid, send a 'fail' response.
        if ($form->isInvalid()) {
            return response([
				'status' => 'fail',
				'messages' => $form->getErrors(),
				'payload' => []
			], 422);
        }

        // If we are still on the go and we have a model
        // let's save data. (if primary key is available
        // it will update otherwise insert).
        if ($form->hasModel()) {
            $form->save();
        }

        // Retrieve form entryId which contains possible
        // primary key for a newly inserted entity.
        $entryId = $form->getUpdates();

        //Send thank you email to entrant
        Mail::to($request->data['email'])->send(new EntryMail($request->data, $entryId));

		// Finally send a response.
		return response([
			'status' => 'success',
			'messages' => [],
			'payload' => count($entryId) > 0 ? [
				'updates' => $entryId
			] : []
		], 200);
	}

	public function show($id)
	{
		return view('pages.entry.view', [
			'entry' => Entry::whereId($id)->with('school')->first(),
		]);
	}
}
