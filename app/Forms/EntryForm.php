<?php


namespace App\Forms;


use App\Models\Entry;
use App\Models\School;
use App\Models\SchoolYear;
use Illuminate\Support\Str;
use Laraform\Laraform;

class EntryForm extends Laraform
{
	public $component = 'entry-form';

	public $model = Entry::class;

	private $standardClasses = [
		'elementContainer' => 'input',
		'elementInner' => 'input__inner',
		'labelContainer' => 'input__label',
		'fieldContainer' => 'input__field',
	];

	public function getEndpoint()
	{
		return route('store');
	}

	public function schema() {


		return [
			'id' => [
				'type' => 'key',
				'secret' => false
			],
			'name' => [
				'type' => 'text',
				'label' => 'Name',
				'rules' => 'required',
				'messages' => [
					'required' => 'Please enter your name',
				],
				'classes' => $this->standardClasses,
			],
			'surname' => [
				'type' => 'text',
				'label' => 'Surname',
				'rules' => 'required',
				'messages' => [
					'required' => 'Please enter your surname',
				],
				'classes' => $this->standardClasses,
			],
			'email' => [
				'type' => 'text',
				'label' => 'Email',
				'rules' => ['required', 'email'],
				'messages' => [
					'required' => 'We need an email address so we can contact you if you win',
				],
				'description' => 'If you’re not sure what to put here ask a parent or teacher.',
				'classes' => $this->standardClasses,
				'debounce' => 300
			],
			'title' => [
				'type' => 'text',
				'label' => 'Title',
				'rules' => 'required',
				'messages' => [
					'required' => 'Your story needs a name',
				],
				'classes' => $this->standardClasses,
			],
			'story' => [
				'type' => 'textarea',
				'label' => 'Story',
				'autogrow' => true,
				'rules' => 'required',
				'messages' => [
					'required' => 'You can’t enter without a story',
				],
				'classes' => [
					'elementContainer' => 'input',
					'elementInner' => 'input__inner input__inner--wide',
					'labelContainer' => 'input__label',
					'fieldContainer' => 'input__field',
				],
			],
			'school_id' => [
				'type' => 'select',
				'label' => 'School',
				'items' => School::orderBy('name')->get()->transform(function($item) {
					return [
						'label' => $item->name,
						'value' => $item->id,
					];
				})->toArray(),
				'rules' => 'required',
				'classes' => $this->standardClasses,
			],
			'school_year_id' => [
				'type' => 'select',
				'label' => 'School year',
				'items' => SchoolYear::all()->pluck('year', 'id')->toArray(),
				'rules' => 'required',
				'classes' => $this->standardClasses,
			],
			'slug' => [
				'type' => 'hidden'
			]
		];
	}

	/**
	 * Runs before validation and creates a slug for the entry
	 */
	public function validating() {
		$this->data['slug'] = Str::slug($this->data['title']);
	}
}
