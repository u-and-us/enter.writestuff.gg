<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
	public function entries() {
		return $this->hasMany(Entry::class);
	}
}
