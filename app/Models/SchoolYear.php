<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
	public function entries() {
		return $this->hasMany(Entry::class);
	}

	public function getCategoryAttribute() {
		if ($this->id >= 1 && $this->id <= 4) {
			return 'Primary';
		} elseif ($this->id >= 5 && $this->id <= 7) {
			return 'Intermediate';
		} else {
			return 'Senior';
		}
	}
}
