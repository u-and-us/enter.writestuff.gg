<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Entry extends Model
{
    protected $guarded = [];

    public function school() {
    	return $this->belongsTo(School::class);
	}

    public function schoolYear() {
    	return $this->belongsTo(SchoolYear::class);
	}

    public function judge() {
    	return $this->belongsTo(Judge::class);
	}

    public function reader() {
    	return $this->belongsTo(Judge::class, 'reader_id');
	}

	public function getFullNameAttribute() {
    	return ucwords($this->name) . ' ' . ucwords($this->surname);
	}

	public function getSlugAttribute() {
    	return Str::slug($this->title_edited);
	}

	public function getStoryHtmlAttribute() {
    	$paragraphs = explode("\r\n", $this->story_edited);

    	return '<p>' . implode('</p><p>', $paragraphs) . '</p>';
	}
}
